# Valgrind on Alpine Test

Trying to setup automatic memory leak tests on gitlab-ci I noticed that Valgrind have some problems with musl libc.

## Test
```
./configure
cd build
make all
make test_memcheck
```

On my machine (Manjaro with gcc and glibc) it doesn't flag any memory leak.

/**
 * @file vector.h
 * @author Davide Peressoni
 * @brief Library for managing automatic growing vectors.
 * @version 1.0.0
 * @date 2020-05-08
 *
 * @copyright Copyright (c) 2020
 */

#ifndef _VECTOR
#define _VECTOR

#include<stddef.h>
#include<inttypes.h>

/// Generic growable vecotr.
typedef struct{
  /// Pointer to first vector element
  void *p;
  /// Size of a vector element
  size_t element_size;
  /// Number of elements in the vector
  size_t size;
  /// Number of elementes the vector can contain
  size_t capacity;
} vector_t;

/**
 * @brief Allocates memory for the given vector.
 *
 * @param v Vector to allocate.
 *
 * Not required if the init capacity is zero.
 */
void vector_allocate(vector_t *v);

/**
 * @brief Deallocates a given vector.
 *
 * @param v Vector to deallocate.
 */
void vector_deallocate(vector_t *v);

/**
 * @brief Returns an element from a vector.
 *
 * @param v Vector.
 * @param index Element index.
 * @return void* Pointer to the choosen element.
 */
void *vector_at(vector_t *v, size_t index);

/**
 * @brief Push a new element in the end of the given vector.
 *
 * @param v Vector.
 * @param e Pointer to element to push.
 * @return void* Pointer to the inserted element itno the vector.
 */
void *vector_push(vector_t *v, void *e);


#endif

cmake_minimum_required(VERSION 3.0.0)
project(ValgrindTest VERSION 1.0.0 LANGUAGES C)
set(CMAKE_C_STANDARD 99)

set(SRC_FOLDER src)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib/static)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# BUILD TARGET

add_subdirectory (${SRC_FOLDER})


# TEST TARGET

include (CTest)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/test)
add_subdirectory (test)
add_custom_target(
  test_memcheck
  COMMAND ${CMAKE_CTEST_COMMAND} --force-new-ctest-process --test-action memcheck # Do test and memory check
  COMMAND cat "${CMAKE_BINARY_DIR}/Testing/Temporary/MemoryChecker.*.log" # Print Valgrind output
)

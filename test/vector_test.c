/**
 * @file vector_test.c
 * @author Davide Peressoni
 * @brief Unit test for vector library.
 * @version 1.0.0
 * @date 2020-05-13
 *
 * @copyright Copyright (c) 2020
 */

#include<assert.h>

#include "../src/vector/vector.h"

#define ARR_SIZE(A) (sizeof(A)/sizeof(*(A)))

int main(){

  // Storing elements of the same type

  int source[] = {1,4,6,2,7,35354,-554753,7667876,454766,-674563476,454657,-35486836,56465753,1,-5,73,634};

  vector_t a = {.element_size = sizeof(int)};

  for(size_t i = 0; i < ARR_SIZE(source); ++i){
    assert(a.size == i);

    vector_push(&a, source+i);

    assert(*(int*)vector_at(&a, i) == source[i]);
  }

  vector_deallocate(&a);


  return 0;
}
